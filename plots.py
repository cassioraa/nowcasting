# figures
import matplotlib.mlab as ml
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib import ticker
import matplotlib.ticker as plticker
import palettable
import seaborn as sns
import numpy as np
import pandas as pd 
import matplotlib.pyplot as plt
from statsmodels.tsa import stattools


# import pymc3 as pm

def stdfigsize(scale=1, nx=1, ny=1, ratio=1.3):
    """
    Returns a tuple to be used as figure size.
    -------
    returns (7*ratio*scale*nx, 7.*scale*ny)
    By default: ratio=1.3
    If ratio<0 them ratio = golden ratio
    """
    if ratio < 0:
        ratio = 1.61803398875
    return((7*ratio*scale*nx, 7*scale*ny))

def stdrcparams(usetex=True):
    """
    Set several mpl.rcParams and sns.set_style for my taste.
    ----
    usetex = True
    ----
    """
    sns.set_style("white")
    sns.set_style({"xtick.direction": "out",
                 "ytick.direction": "out"})
    rcparams = {
    'text.usetex': usetex,
    'font.family': 'sans-serif',
    'font.sans-serif': ['Times'],
    'axes.labelsize': 35,
    'axes.titlesize': 35,
    'legend.fontsize': 20,
    'ytick.right': 'off',
    'xtick.top': 'off',
    'ytick.left': 'on',
    'xtick.bottom': 'on',
    'xtick.labelsize': '25',
    'ytick.labelsize': '25',
    'axes.linewidth': 2.5,
    'xtick.major.width': 1.8,
    'xtick.minor.width': 1.8,
    'xtick.major.size': 14,
    'xtick.minor.size': 7,
    'xtick.major.pad': 10,
    'xtick.minor.pad': 10,
    'ytick.major.width': 1.8,
    'ytick.minor.width': 1.8,
    'ytick.major.size': 14,
    'ytick.minor.size': 7,
    'ytick.major.pad': 10,
    'ytick.minor.pad': 10,
    'axes.labelpad': 15,
    'axes.titlepad': 15,
    'axes.spines.right': False,
    'axes.spines.top': False}

    mpl.rcParams.update(rcparams) 
    mpl.rcParams['lines.linewidth'] = 5
    mpl.rcParams['pdf.fonttype'] = 42 

stdrcparams(usetex=True)
figsize=stdfigsize(ratio=-1)
xs,ys=figsize

def lighten_color(color, amount=0.5):
    """
    Lightens the given color by multiplying (1-luminosity) by the given amount.
    Input can be matplotlib color string, hex string, or RGB tuple.
    Parameters
    ----------
    color: the color variable (e.g. 'g', '#F034A3', (.3,.55,.1)).
    amount: the lighten amount factor (Default: 0.5)
    ----------
    returns modified color
    """
    import matplotlib.colors as mc
    import colorsys
    try:
        c = mc.cnames[color]
    except:
        c = color
    c = colorsys.rgb_to_hls(*mc.to_rgb(c))
    return mpl.colors.to_hex(colorsys.hls_to_rgb(c[0], 1 - amount * (1 - c[1]), c[2]))
    
def get_axis_limits(ax, scale=1.1):
    return ax.get_xlim()[1]*scale, ax.get_ylim()[1]*scale




def plot_acf(x, lag, axis, param_name, title=None):
    acf  = stattools.acf(x, nlags=lag, fft=True)
#     pacf = stattools.pacf(x, nlags=lag)

    axis.bar(range(len(acf)), acf, color = '#373aa1', alpha=0.5, label=param_name)
    axis.axhline(1/((len(x)**0.5)), linestyle='--', lw=1, color='#b32e2e')
    axis.axhline(0, color='k', lw=0.5)
    axis.axhline(-1/((len(x)**0.5)), linestyle='--', lw=1, color='#b32e2e')
    axis.set_xlabel('Lags')
    axis.set_ylabel('acf')
    axis.legend(loc="center")
    
    if title is not None:
        axis.set_title(title)

        
def plot_pacf(x, lag, axis, param_name, title=None):
    # acf  = stattools.acf(x, nlags=lag, fft=True)
    pacf = stattools.pacf(x, nlags=lag)

    axis.bar(range(len(pacf)), pacf, color = '#373aa1', alpha=0.5, label=param_name)
    axis.axhline(1/((len(x)**0.5)), linestyle='--', lw=1, color='#b32e2e')
    axis.axhline(0, color='k', lw=0.5)
    axis.axhline(-1/((len(x)**0.5)), linestyle='--', lw=1, color='#b32e2e')
    axis.set_xlabel('Lags')
    axis.set_ylabel('pacf')
    axis.legend(loc="center")
    
    if title is not None:
        axis.set_title(title)
