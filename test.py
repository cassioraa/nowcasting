import numpy as np
import pandas as pd
from hs_gibbs import HorseshoeRegression
import matplotlib.pyplot as plt
import seaborn as sns

# Set random seed for reproducibility
np.random.seed(0)

# Generate sample data
n = 100  # Number of observations
p = 10   # Number of predictors
X = pd.DataFrame(np.random.randn(n, p), columns=[f'X{i}' for i in range(p)])
true_beta = np.array([3, 0, 2, 0, -1, 0, 0, 0, 0, 0])
y = pd.Series(X.values @ true_beta + np.random.randn(n), name='y')

# Separate the last 10 observations as test set
X_train = X.iloc[:-5]
y_train = y.iloc[:-5]
X_test = X.iloc[-5:]
y_test = y.iloc[-5:]

# List to store forecasts
forecasts_density = {}
forecast_ols = {}
model = HorseshoeRegression(nsamps=1_000, nburn=1_000)

# Perform expanding window estimation and density forecasting
for i in range(5, 1, -1):
    ii = -i 
    print(ii)
    X_train = X.iloc[:ii]
    y_train = y.iloc[:ii]

    # Fit the model
    model.fit(X_train, y_train)

    # Forecast density for the next value
    X_next = X.iloc[ii:ii+1]  # Next X value to forecast
    density_forecast = model.predict_density(X_next)

    # Store the density forecast
    forecasts_density[y.index[ii+1]] = density_forecast

    forecast_ols[y.index[ii+1]] = (X_next @ np.linalg.solve(X_train.T @ X_train, X_train.T @ y_train)).values
# Print the density forecasts
print("Density Forecasts:", forecasts_density)


# Convert forecasts to DataFrame
forecasts_density = {k:v.flatten() for k, v in forecasts_density.items()}
forecasts_df = pd.DataFrame.from_dict(forecasts_density)
ols_forecasts_df = pd.DataFrame(forecast_ols)

# Plot density forecast as a violin plot
fig, ax = plt.subplots()
sns.violinplot(data=forecasts_df, inner='point', linewidth=1, ax=ax,  scale='width')
ax.set_title('Density Forecast as Violin Plot')

# Plot actual y values for test set only
ax.plot(y_test.reset_index(drop=True).iloc[:-1], 'ro-', label='Test y')
ax.plot(ols_forecasts_df.T.reset_index(drop=True), 'bo-', label='OLS')
ax.plot(forecasts_df.quantile(0.5).reset_index(drop=True), 'go-', label='Median')

ax.fill_between(
    x=forecasts_df.quantile(0.5).reset_index(drop=True).index, 
    y1=forecasts_df.quantile(0.05), 
    y2=forecasts_df.quantile(0.95), 
    alpha=0.5, 
    color='tomato')


ax.set_xlabel('Observations')
ax.set_ylabel('Values')
ax.legend()
plt.tight_layout()
plt.show()

