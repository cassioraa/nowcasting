import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import seaborn as sns

dates = pd.date_range(start="2023-01-01", periods=100)  # Example date range
x = np.random.normal(size=(100, 10))
df = pd.DataFrame(x, columns=["x" + str(i + 1) for i in range(10)], index=dates)

fig, ax = plt.subplots(nrows=10, figsize=(10, 5))
plt.subplots_adjust(hspace=0.0)

# Adjust the first date slightly to start from 2023
offset_dates = dates - pd.Timedelta(days=20)

for i in range(10):
    sns.heatmap(df.T.iloc[[i]], ax=ax[i], cbar=False)  # Transpose the DataFrame
    
    if i < 9:
        ax[i].set_xticks([])  # Exclude ticks and labels for x-axis
    else:
        ax[i].xaxis.set_major_locator(mdates.YearLocator())  # Set locator to show years
        ax[i].xaxis.set_major_formatter(mdates.DateFormatter('%Y-%m-%d'))  # Format the x-axis labels
        ax[i].set_xticklabels(offset_dates.strftime('%Y-%m-%d'))  # Apply formatted dates

# Adjust hspace between groups
for i in range(3):
    ax[i].set_position(ax[i].get_position().translated(0, 0.2))

for i in range(3, 6):
    ax[i].set_position(ax[i].get_position().translated(0, 0.1))

ax[0].set_title("Subtitle 1 here", loc='left')
ax[3].set_title("Subtitle 2 here", loc='left')
ax[6].set_title("Subtitle 3 here", loc='left')

# Add an annotation for the general title
plt.annotate("General Title", xy=(0.5, 0.98), xycoords="figure fraction",
             ha="center", fontsize=16)

plt.show()


#=====================

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

def create_grouped_heatmap_subplots(dataframe, division, subtitles, general_title):
    n_rows = len(dataframe)
    
    fig, ax = plt.subplots(nrows=n_rows, figsize=(10, 5))
    plt.subplots_adjust(hspace=0.0)

    for i in range(n_rows):
        sns.heatmap(dataframe.iloc[[i]].T, ax=ax[i], cbar=False)  # Transpose the DataFrame

        if i < n_rows - 1:
            ax[i].set_xticks([])  # Exclude ticks and labels for x-axis
        else:
            ax[i].xaxis.set_major_locator(plt.MaxNLocator(integer=True))  # Set locator to show integer years
            ax[i].set_xticklabels(dataframe.columns, rotation=45, ha='right')  # Apply column labels
            
        ax[i].set_yticks([])  # Exclude ticks and labels for y-axis

        # Adjust hspace between groups
        if i in division:
            ax[i].set_position(ax[i].get_position().translated(0, 0.1))
    
        # Set subtitles
        if i < len(subtitles):
            ax[i].set_title(subtitles[i], loc='left')

    # Add an annotation for the general title
    plt.annotate(general_title, xy=(0.5, 0.98), xycoords="figure fraction",
                 ha="center", fontsize=16)

    plt.show()

# Example usage
x = np.random.normal(size=1000)
x = x.reshape((100, 10))
df = pd.DataFrame(x, columns=["x" + str(i + 1) for i in range(10)])
division = [3, 6]
subtitles = ["Subtitle 1 here", "Subtitle 2 here", "Subtitle 3 here"]
general_title = "General Title"

create_grouped_heatmap_subplots(df, division, subtitles, general_title)

