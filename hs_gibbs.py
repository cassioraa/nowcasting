import numpy as np
import pandas as pd
from scipy.stats import invgamma, norm, uniform, expon, gamma
from scipy.linalg import cholesky
from tqdm import tqdm
import matplotlib.pyplot as plt

class HorseshoeRegression:
    def __init__(
            self, 
            nsamps: int = 1000,
            nburn: int = 500,
            a: float = 1.0,
            b: float = 1,
            scale_sigma_prior: bool = True
        ):
        self.nsamps = nsamps
        self.nburn = nburn
        self.a = a
        self.b = b
        self.scale_sigma_prior = scale_sigma_prior
        self.beta_samples = None
        self.lambda_samples = None
        self.sigma_samples = None
        self.tau_samples = None

    def _sample_beta(self, mu_n, chol_Lambda_n_inv, sigma, p, scale_sigma_prior):
        eps = norm.rvs(size=p)
        beta_sample = sigma * chol_Lambda_n_inv @ eps + mu_n if scale_sigma_prior else sigma * chol_Lambda_n_inv @ eps + mu_n
        return beta_sample

    def _sample_sigma(self, a_n, b_0, YY, mu_n, Lambda_n):
        b_n = b_0 + 0.5 * (YY - mu_n.T @ Lambda_n @ mu_n).item()
        sigma2_sample = invgamma.rvs(a=a_n, scale=b_n)
        return np.sqrt(sigma2_sample)

    def _sample_lambda(self, lambda_prev, beta, sigma, tau, p, scale_sigma_prior):
        lambda_new = np.zeros(p)
        mu2_j = (beta / (sigma * tau)) ** 2 if scale_sigma_prior else (beta / tau) ** 2

        for j in range(p):
            rate_lambda = mu2_j[j] / 2.0
            gamma_l = 1.0 / lambda_prev[j] ** 2
            u1 = uniform.rvs(0, 1.0 / (1.0 + gamma_l))
            trunc_limit = (1.0 - u1) / u1
            ub_lambda = expon.cdf(trunc_limit, scale=1.0 / rate_lambda)
            u2 = uniform.rvs(0, ub_lambda)
            gamma_l = expon.ppf(u2, scale=1.0 / rate_lambda)
            lambda_new[j] = 1.0 / np.sqrt(gamma_l)
        return lambda_new

    def _sample_tau(self, lambda_curr, beta, sigma, tau_prev, scale_sigma_prior):
        shape_tau = 0.5 * (1.0 + len(lambda_curr))
        gamma_tt = 1.0 / tau_prev ** 2
        u1 = uniform.rvs(0, 1.0 / (1.0 + gamma_tt))
        trunc_limit_tau = (1.0 - u1) / u1
        mu2_tau = np.sum((beta / (sigma * lambda_curr)) ** 2) if scale_sigma_prior else np.sum((beta / lambda_curr) ** 2)
        rate_tau = mu2_tau / 2.0
        ub_tau = gamma.cdf(trunc_limit_tau, shape_tau, scale=1.0 / rate_tau)
        u2 = uniform.rvs(0, ub_tau)
        gamma_tt = gamma.ppf(u2, shape_tau, scale=1.0 / rate_tau)
        tau_new = 1.0 / np.sqrt(gamma_tt)
        return tau_new

    def fit(self, X, y):
        """
        Fit the horseshoe regression model using Gibbs sampling.
        
        Parameters:
            X (pd.DataFrame): Predictor variables.
            y (pd.Series): Response variable.
        """
        X = X.values
        y = y.values

        n, p = X.shape
        a_0, b_0 = self.a, self.b

        beta = np.zeros((self.nsamps + self.nburn, p))
        sigma = np.zeros(self.nsamps + self.nburn)
        lambda_ = np.ones((self.nsamps + self.nburn, p))
        tau = np.ones(self.nsamps + self.nburn)

        sigma[0] = 1.0
        tau[0] = 1.0

        sdy = np.std(y)
        sdx = np.std(X, axis=0)
        y = y / sdy
        X = (X - np.mean(X, axis=0)) / np.std(X, axis=0)

        XX = X.T @ X
        XY = X.T @ y
        YY = y.T @ y

        a_n = a_0 + n / 2.0

        for i in tqdm(range(1, self.nsamps + self.nburn), desc="Gibbs Sampling"):
            Lambda0_diag = 1.0 / (lambda_[i-1]**2 * tau[i-1]**2)
            Lambda_n = XX + np.diag(Lambda0_diag)
            Lambda_n_inv = np.linalg.inv(Lambda_n)
            chol_Lambda_n_inv = cholesky(Lambda_n_inv, lower=True)
            mu_n = Lambda_n_inv @ XY

            beta[i] = self._sample_beta(mu_n, chol_Lambda_n_inv, sigma[i-1], p, self.scale_sigma_prior)
            sigma[i] = self._sample_sigma(a_n, b_0, YY, mu_n, Lambda_n)
            lambda_[i] = self._sample_lambda(lambda_[i-1], beta[i], sigma[i], tau[i-1], p, self.scale_sigma_prior)
            tau[i] = self._sample_tau(lambda_[i], beta[i], sigma[i], tau[i-1], self.scale_sigma_prior)

        for ll in range(beta.shape[0]):
            beta[ll] = beta[ll] / sdx * sdy

        self.beta_samples = beta[self.nburn:]
        self.lambda_samples = lambda_[self.nburn:]
        self.sigma_samples = sigma[self.nburn:]
        self.tau_samples = tau[self.nburn:]

    def predict(self, X_new):
        """
        Make predictions using the fitted horseshoe regression model.
        
        Parameters:
            X_new (pd.DataFrame): New predictor variables.
            
        Returns:
            np.ndarray: Predicted values.
        """
        if self.beta_samples is None:
            raise ValueError("The model is not fitted yet. Call the fit method first.")

        X_new = X_new.values
        X_new = (X_new - np.mean(X_new, axis=0)) / np.std(X_new, axis=0)

        beta_mean = np.mean(self.beta_samples, axis=0)
        y_pred = X_new @ beta_mean
        return y_pred
    

    def predict_density(self, X_new):
        if self.beta_samples is None:
            raise ValueError("The model is not fitted yet. Call the fit method first.")
        n_obs, p = X_new.shape
        X_new = X_new.values

        y_pred = X_new @ self.beta_samples.T
        return y_pred

if __name__ == '__main__':
        
    # Example usage with plotting
    np.random.seed(0)
    n = 100  # Number of observations
    p = 10   # Number of predictors
    X = pd.DataFrame(np.random.randn(n, p), columns=[f'X{i}' for i in range(p)])
    true_beta = np.array([3, 0, 2, 0, -1, 0, 0, 0, 0, 0])
    y = pd.Series(X.values @ true_beta + np.random.randn(n), name='y')

    # Initialize the model
    model = HorseshoeRegression(nsamps=1000)

    # Fit the model
    model.fit(X, y)

    # Make density forecasts
    X_new = pd.DataFrame(np.random.randn(5, p), columns=[f'X{i}' for i in range(p)])
    predictions_density = model.predict_density(X_new)

    # Plot density forecasts
    plt.figure(figsize=(10, 6))
    for i in range(predictions_density.shape[0]):
        sns.kdeplot(predictions_density[i, :], label=f'Predictor {i}')
    plt.title('Density Forecasts for Predictors')
    plt.xlabel('Forecasted Values')
    plt.ylabel('Density')
    plt.legend()
    plt.show()