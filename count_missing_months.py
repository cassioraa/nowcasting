import pandas as pd

def count_missing_months(monthly_data, quarterly_data):
    """
    Counts the number of missing months within the last quarter and the last month.

    Args:
        monthly_data (pandas.DataFrame): The monthly data containing dates as keys or as a DataFrame index.
        quarterly_data (pandas.DataFrame): The quarterly data containing dates as keys or as a DataFrame index.

    Returns:
        tuple: A tuple containing the number of observed months, the number of missing months, and the last quarter.

    Raises:
        ValueError: If the monthly_data or quarterly_data is in an invalid format.

    Examples:
        # Sample monthly and quarterly data
        monthly_data = pd.DataFrame(
            {
            '2023-01': [10, 20, 30],
            '2023-02': [15, 25, 35],
            '2023-03': [12, 22, 32],
            '2023-04': [18, 28, 38],
            '2023-05': [11, 21, 31]
            })
        quarterly_data = pd.DataFrame(
            {
            '2022Q4': [100, 200, 300],
            '2023Q1': [150, 250, 350],
            '2023Q2': [120, 220, 320]
            })
        
        # Set index as date
        quarterly_data = pd.DataFrame(quarterly_data).T
        quarterly_data.index=pd.to_datetime(quarterly_data.index)

        monthly_data = pd.DataFrame(monthly_data).T
        monthly_data.index=pd.to_datetime(monthly_data.index)

        # Count missing months
        missing_months = count_missing_months(monthly_data, quarterly_data)
        print("Missing months:", missing_months)       # Output: 1
    """

    # Get the last quarter from the quarterly data
    last_quarter = quarterly_data.index.to_period('Q')[-1]

    # Get the last month from the monthly data
    last_month = monthly_data.index.to_period('M')[-1]

    # Count the number of observed months within the last quarter
    observed_months = len(monthly_data[monthly_data.index.to_period('Q') == last_quarter])

    # Count the number of missing months within the last quarter
    missing_months = 3 - observed_months

    return missing_months
