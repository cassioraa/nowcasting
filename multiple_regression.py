import numpy as np
from scipy.stats import norm, gamma, beta, expon, cauchy, laplace, lognorm, t
from tqdm import tqdm
from joblib import Parallel, delayed

def loglike(param, X, y):
    """
    Compute the log likelihood of the data given the model parameters.

    Parameters:
    - param (array): Array containing regression coefficients and scale parameter.
    - X (array): Design matrix with shape (n_samples, n_regressors).
    - y (array): Dependent variable with shape (n_samples,).

    Returns:
    - float: Log likelihood.
    """
    return np.sum(norm.logpdf(y, loc=np.dot(X, param[:-1]) + param[-1], scale=param[-2]))

def logprior(param, prior_info=None):
    """
    Compute the log prior of the model parameters.

    Parameters:
    - param (array): Array containing regression coefficients and scale parameter.
    - prior_info (dict): A dictionary with prior information. Example: 
        {"beta0": 
            {"Distribution": "normal", "loc": 0, "scale": 10},
         "beta1":
            {"Distribution": "gamma", "shape": 0.25, "scale": 0.25},
         "beta2":
            {"Distribution": "beta", "a": 2, "b": 5},
         "beta3":
            {"Distribution": "inverse_gamma", "shape": 3, "scale": 1},
         "beta4":
            {"Distribution": "exponential", "scale": 2},
         "beta5":
            {"Distribution": "cauchy", "loc": 0, "scale": 1},
         "beta6":
            {"Distribution": "laplace", "loc": 0, "scale": 1},
         "beta7":
            {"Distribution": "lognorm", "s": 0.954, "scale": np.exp(0.2)},
         "beta8":
            {"Distribution": "t", "df": 3, "loc": 0, "scale": 1}
        }
    Returns:
    - float: Log prior.
    """

    if prior_info is None:
        # Default prior: All beta_i follow a normal distribution, and the last parameter follows a gamma distribution.
        prior_info = {f"beta{i}": {"Distribution": "normal", "loc": 0, "scale": 10} for i in range(len(param) - 1)}
        prior_info["sigma"] = {"Distribution": "gamma", "shape": 2, "scale": 2}


    log_prior = 0
    for k, beta_k in enumerate(prior_info.keys()):

        if prior_info[beta_k]['Distribution'] == 'normal':
            loc = prior_info[beta_k]['loc']
            scale = prior_info[beta_k]['scale']
            log_prior += norm.logpdf(param[k], loc=loc, scale=scale)
        
        elif prior_info[beta_k]['Distribution'] == 'gamma':
            shape = prior_info[beta_k]['shape']
            scale = prior_info[beta_k]['scale']
            log_prior += gamma.logpdf(param[k], a=shape, scale=scale)
        
        elif prior_info[beta_k]['Distribution'] == 'beta':
            a = prior_info[beta_k]['a']
            b = prior_info[beta_k]['b']
            log_prior += beta.logpdf(param[k], a=a, b=b)
        
        elif prior_info[beta_k]['Distribution'] == 'inverse_gamma':
            shape = prior_info[beta_k]['shape']
            scale = prior_info[beta_k]['scale']
            log_prior += gamma.logpdf(1/param[k], a=shape, scale=scale) - 2 * np.log(param[k])
        
        elif prior_info[beta_k]['Distribution'] == 'exponential':
            scale = prior_info[beta_k]['scale']
            log_prior += expon.logpdf(param[k], scale=scale)
        
        elif prior_info[beta_k]['Distribution'] == 'cauchy':
            loc = prior_info[beta_k]['loc']
            scale = prior_info[beta_k]['scale']
            log_prior += cauchy.logpdf(param[k], loc=loc, scale=scale)
        
        elif prior_info[beta_k]['Distribution'] == 'laplace':
            loc = prior_info[beta_k]['loc']
            scale = prior_info[beta_k]['scale']
            log_prior += laplace.logpdf(param[k], loc=loc, scale=scale)
        
        elif prior_info[beta_k]['Distribution'] == 'lognorm':
            s = prior_info[beta_k]['s']
            scale = prior_info[beta_k]['scale']
            log_prior += lognorm.logpdf(param[k], s=s, scale=scale)
        
        elif prior_info[beta_k]['Distribution'] == 't':
            df = prior_info[beta_k]['df']
            loc = prior_info[beta_k]['loc']
            scale = prior_info[beta_k]['scale']
            log_prior += t.logpdf(param[k], df=df, loc=loc, scale=scale)
    
    return log_prior


def logpost(param, X, y):
    """
    Compute the log posterior of the model parameters.

    Parameters:
    - param (array): Array containing regression coefficients and scale parameter.
    - X (array): Design matrix with shape (n_samples, n_regressors).
    - y (array): Dependent variable with shape (n_samples,).

    Returns:
    - float: Log posterior.
    """
    return logprior(param) + loglike(param, X, y)

def metropolis(param0, niter, logpost, X, y, accept_target=0.5, cov=None):
    """
    Run a Metropolis-Hastings Markov Chain Monte Carlo for Bayesian parameter estimation.

    Parameters:
    - param0 (array): Initial parameters of the Metropolis algorithm.
    - niter (int): Number of iterations.
    - logpost (function): Log posterior function.
    - X (array): Design matrix with shape (n_samples, n_regressors).
    - y (array): Dependent variable with shape (n_samples,).
    - accept_target (float): Target acceptance rate.
    - cov (array): Covariance matrix for proposing new parameter values.

    Returns:
    - array: Samples from the posterior distribution.
    """
    if cov is None:
        cov = np.eye(len(param0))

    scale = 2.38 / np.sqrt(len(param0) - 1)

    accept = 0
    samples = np.zeros((1 + niter, len(param0)))
    samples[0, :] = param0

    progress_bar = tqdm(range(1, niter + 1))
    for i in progress_bar:
        proposal = np.random.multivariate_normal(samples[i - 1, :], scale * cov)
        if np.log(np.random.uniform(0, 1)) < (logpost(proposal, X, y) - logpost(samples[i - 1, :], X, y)):
            samples[i, :] = proposal
            accept += 1
        else:
            samples[i, :] = samples[i - 1, :]
        acceptance_rate = accept / i
        if i % 100 == 0 and i < niter * 0.5:
            new_scale = scale * (norm.ppf(accept_target / 2) / norm.ppf(acceptance_rate / 2))
            scale = new_scale if new_scale > 0 else 0.001
        progress_bar.set_postfix({'acceptance rate': acceptance_rate})

    return samples[1:]

def run_chains(niter, number_of_chains, X, y):
    """
    Run multiple chains in parallel using the Metropolis-Hastings algorithm.

    Parameters:
    - number_of_chains (int): Number of chains to run in parallel.
    - X (array): Design matrix with shape (n_samples, n_regressors).
    - y (array): Dependent variable with shape (n_samples,).

    Returns:
    - array: Samples from the posterior distribution.
    """
    param0 = np.random.uniform(size=X.shape[1] + 1, low=0, high=1)
    output = metropolis(param0=param0, niter=niter, logpost=logpost, X=X, y=y, accept_target=0.5, cov=None)
    # print("Acceptance rate for chain {}: {}".format(number_of_chains, acceptance_rate))
    return output

if __name__ == '__main__':

    # Simulate Data
    np.random.seed(42)
    n_samples = 1000
    intercept = np.random.normal(0, 10)
    betas = np.random.beta(1, 1, size=3)
    sigma = np.random.gamma(0.25, 0.25)

    XX = np.random.randn(n_samples, 3)  # 3 independent variables
    y = intercept + np.dot(XX, betas) + np.random.normal(0, sigma, size=n_samples)

    X = np.c_[np.ones((n_samples,1)), XX]

    # Example Usage of Metropolis-Hastings
    number_of_chains = 2
    chains = Parallel(n_jobs=-1)(delayed(run_chains)(50000, i, X, y) for i in range(number_of_chains))

    # Extracting Results
    burn_in = 100  # Discard initial samples as burn-in
    samples1 = chains[0][burn_in:]
    samples2 = chains[1][burn_in:]


    # Print Summary Statistics or Perform Further Analysis
    print("Summary Statistics of Beta Parameters:")
    print(np.mean(samples1, axis=0))
    print(np.std(samples1, axis=0))

    fig, ax = plt.subplots(nrows=2, ncols=2, figsize=stdfigsize(nx=2, ny=1, scale=1.5))

    k = 0
    for i in range(2):
        for j in range(2): 
            ax[i,j].plot(chains[0][burn_in:,k])
            ax[i,j].axhline(np.r_[intercept, betas][k], color='r')

            k+=1
    plt.show()